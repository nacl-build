
import optparse
import os
import posixpath
import re
import signal
import subprocess
import sys
import tempfile

import action_tree
import cmd_env
import deps_update


# Work out tar/Python bug.
# See http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=532570
# and http://bugs.python.org/issue1652
signal.signal(signal.SIGPIPE, signal.SIG_DFL)


urls = {
    "python": "http://www.python.org/ftp/python/2.6.1/Python-2.6.1.tar.bz2",
    }

svn_repos = [
    ("http://nativeclient.googlecode.com/svn/trunk/src/third_party@1108",
     "third_party"),
    ("http://nativeclient.googlecode.com/svn/trunk/src/native_client/src/third_party/npapi@1424",
     "native_client/src/third_party/npapi"),
    ]


def stamp_file(filename):
    fh = open(filename, "w")
    fh.close()


def is_elf_file(filename):
    fh = open(filename)
    header = fh.read(4)
    fh.close()
    return header == "\x7fELF"


def assert_equals(x, y):
    if x != y:
        if type(x) == str and type(y) == str:
            raise AssertionError("ACTUAL:\n%s\nEXPECTED:\n%s" % (x, y))
        raise AssertionError("%r != %r" % (x, y))


# This is an alternative to subprocess.PIPE, which can cause deadlocks
# if the pipe buffer gets filled up.
def make_fh_pair():
    # Make read/write file handle pair.  This is like creating a pipe
    # FD pair, but without a pipe buffer limit.
    fd, filename = tempfile.mkstemp(prefix="nacl_test_")
    try:
        write_fh = os.fdopen(fd, "w", 0)
        read_fh = open(filename, "r")
    finally:
        os.unlink(filename)
    return write_fh, read_fh


# Workaround for Python's variable binding semantics.
def make_action(func, *args):
    return lambda log: func(*args)


class NaClBuild(object):

    def __init__(self, env, dir_path):
        self._env = env
        self._dir_path = dir_path
        self._glibc_src = "glibc"
        # This is the directory where scons looks for the toolchain by
        # default.
        self._sdkloc = os.path.join(
            self._dir_path, "native_client/src/third_party/nacl_sdk/linux/sdk")

        def add_path(subdirs):
            path = ":".join(
                [os.path.join(self._dir_path, subdir) for subdir in subdirs] +
                [os.environ["PATH"]])
            return ["env", "PATH=%s" % path]

        subdirs = ["native_client/scons-out/dbg-linux-x86-32/staging",
                   os.path.join(self._sdkloc, "nacl-sdk/bin"),
                   "bin"]
        self._path_env = cmd_env.PrefixCmdEnv(add_path(subdirs), env)
        self._gcc_override_env = cmd_env.PrefixCmdEnv(
            add_path(subdirs + ["override-bin"]), env)
        self.path_env = self._path_env

    def checkout_thirdparty(self, log):
        for url, dest in svn_repos:
            self._env.cmd(["svn", "checkout", url, dest])

    def svn_checkout(self, log):
        self._env.cmd(["python", "mini_gclient.py"])

    def _git_fetch_repo(self, dir_path, url):
        # This is like "git clone --no-checkout", but it is more incremental.
        git_dir = os.path.join(dir_path, ".git")
        if not os.path.exists(git_dir):
            self._env.cmd(["mkdir", "-p", dir_path])
            self._env.cmd(cmd_env.in_dir(dir_path) +
                          ["git", "init"])
            self._env.cmd(cmd_env.in_dir(dir_path) +
                          ["git", "remote", "add", "origin", url])
        refspec = "+refs/heads/*:refs/remotes/origin/*"
        self._env.cmd(cmd_env.in_dir(dir_path) +
                      ["git", "fetch", url, refspec])

    def _git_checkout_repo(self, dir_path, commit):
        # This will leave HEAD set to a commit ID, not a branch.
        # Be careful.  One could lose commits that are not saved
        # in branches.
        self._env.cmd(cmd_env.in_dir(dir_path) +
                      ["git", "checkout", commit])

    @action_tree.action_node
    def git_fetch(self):
        for mod in deps_update.get_modules():
            yield mod["name"], make_action(
                self._git_fetch_repo, mod["path"], mod["url"])

    @action_tree.action_node
    def git_checkout(self):
        for mod in deps_update.get_modules():
            yield mod["name"], make_action(
                self._git_checkout_repo, mod["path"], mod["commit"])

    def make_stamps(self, log):
        # Stops tools/Makefile deleting our source and Git repos.
        assert os.path.exists("native_client/tools/BUILD/binutils-2.20")
        stamp_file("native_client/tools/BUILD/stamp-binutils-2.20")

    def fetch_sqlite(self, log):
        self._env.cmd(["mkdir", "-p", "debs"])
        url = "http://archive.ubuntu.com/ubuntu/pool/main/s/sqlite3/sqlite3_3.6.10-1.dsc"
        # Disable signature checking because it doesn't work under
        # karmic where the public key is apparently not available.
        # Our SVN downloads are not secure so this is not a big deal.
        # We'd really want to include a hash here anyway, not a public
        # key check.
        # TODO: do a secure download.
        self._env.cmd(cmd_env.in_dir("debs") +
                      ["dget", "--allow-unauthenticated", "-x", url])

    def fetch_python(self, log):
        self._env.cmd(["wget", "-c", urls["python"],
                       "-O", posixpath.basename(urls["python"])])

    def unpack_python(self, log):
        self._env.cmd(["tar", "-xjf", posixpath.basename(urls["python"])])

    @action_tree.action_node
    def checkout(self):
        return [self.checkout_thirdparty,
                self.git_fetch,
                self.make_stamps,
                self.git_checkout,
                self.svn_checkout,
                self.fetch_sqlite,
                self.fetch_python,
                self.unpack_python]

    def build_toolchain(self, log):
        self._env.cmd(cmd_env.in_dir("native_client/tools")
                      + ["make", "SDKLOC=%s" % self._sdkloc])

    def build_nacl(self, log):
        self._env.cmd(cmd_env.in_dir("native_client")
                      + ["./scons", "--mode=dbg-linux"])

    def test_nacl(self, log):
        # TODO: use --mode=dbg-linux,nacl
        self._env.cmd(
            cmd_env.in_dir("native_client")
            + ["./scons", "--mode=dbg-linux", "run_all_tests"])

    def configure_glibc(self, log):
        self._path_env.cmd(cmd_env.in_dir(self._glibc_src)
                           + ["./myconfig.sh"])

    def build_glibc(self, log):
        self._env.cmd(cmd_env.in_dir("glibc/build")
                      + ["make"])

    def test_glibc_static(self, log):
        self._path_env.cmd(cmd_env.in_dir(self._glibc_src)
                           + ["./make-example.sh"])
        self._path_env.cmd(
            ["ncval_stubout", "glibc/hellow"])
        write_fh, read_fh = make_fh_pair()
        proc = self._path_env.cmd(["sel_ldr", "-d", "glibc/hellow"],
                                  stdout=write_fh)
        assert_equals(read_fh.read(),
                      "Hello (via write())\n"
                      "Hello world (via printf())\n")

    def _run_dynamic(self, args, **kwargs):
        return self._path_env.cmd(
            ["sel_ldr",
             "-d", "install-stubout/lib/ld-linux.so.2",
             "--", "--library-path", "install-stubout/lib"]
            + args, **kwargs)

    def test_glibc_dynamic(self, log):
        self._path_env.cmd(cmd_env.in_dir(self._glibc_src)
                           + ["./make-example-dynamic.sh"])
        write_fh, read_fh = make_fh_pair()
        proc = self._run_dynamic(["glibc/hellow-dyn"], stdout=write_fh)
        assert_equals(read_fh.read(),
                      "Hello (via write())\n"
                      "Hello world (via printf())\n")

    def test_running_ldso(self, log):
        # TODO: expect a non-zero exit code.
        write_fh, read_fh = make_fh_pair()
        try:
            self._run_dynamic([], stderr=write_fh)
        except cmd_env.CommandFailedError, exn:
            assert_equals(exn.rc, 127)
        else:
            raise AssertionError("Expected non-zero exit code")
        output = read_fh.read()
        print output,
        # Check for ld.so's help message.
        assert ("You have invoked `ld.so', the helper program "
                "for shared library executables." in output)

    def install_glibc(self, log):
        self._env.cmd(cmd_env.in_dir("glibc/build")
                      + ["make", "install"])

    def stub_out(self, log):
        self._env.cmd(["rm", "-rf",
                       "install-stubout"])
        self._env.cmd(["cp", "-a",
                       "install",
                       "install-stubout"])
        self._path_env.cmd(
            ["sh", "-c",
             "ncval_stubout install-stubout/lib/*.so.*"])

    def build_sqlite(self, log):
        self._gcc_override_env.cmd(
            cmd_env.in_dir("debs/sqlite3-3.6.10")
            + ["dpkg-buildpackage", "-b", "-us", "-uc", "-rfakeroot"])

    def configure_python(self, log):
        self._gcc_override_env.cmd(cmd_env.in_dir("Python-2.6.1")
                                   + ["i386", "./configure"])

    def build_python(self, log):
        self._gcc_override_env.cmd(
            cmd_env.in_dir("Python-2.6.1") + ["make"])

    def _get_install_dir(self, subdir_name):
        install_dir = os.path.join(self._dir_path, "install-trees", subdir_name)
        self._env.cmd(["rm", "-rf", install_dir])
        self._env.cmd(["mkdir", "-p", install_dir])
        return install_dir

    def _rewrite_for_nacl(self, install_dir):
        for dir_path, dirnames, filenames in os.walk(install_dir):
            for filename in filenames:
                pathname = os.path.join(dir_path, filename)
                if is_elf_file(pathname):
                    self._path_env.cmd(["ncrewrite", pathname])

    def install_python(self, log):
        install_dir = self._get_install_dir("python")
        self._gcc_override_env.cmd(
            cmd_env.in_dir("Python-2.6.1")
            + ["make", "install", "DESTDIR=%s" % install_dir])
        self._rewrite_for_nacl(install_dir)

    def install_sqlite(self, log):
        install_dir = self._get_install_dir("sqlite")
        self._env.cmd(["dpkg-deb", "-x", "debs/libsqlite3-0_3.6.10-1_i386.deb",
                       install_dir])
        self._rewrite_for_nacl(install_dir)

    def build_python_extension(self, log):
        self._env.cmd(
            ["rm", "-rf", "googleclient/native_client/python_extension/build"])
        self._gcc_override_env.cmd(
            cmd_env.in_dir("googleclient/native_client/python_extension") +
            ["python", "setup.py", "install", "--install-lib=../imcplugin"])
        self._path_env.cmd(
            ["ncrewrite", "googleclient/native_client/imcplugin/nacl.so"])

    def test_libraries(self, log):
        # Minimal library test.  Tests that we can load more libraries
        # than just libc.so, and that libraries can call via the PLT.
        gcc = "nacl-glibc-gcc"
        self._path_env.cmd([gcc, "-shared", "-fPIC", "tests/libhello.c",
                            "-o", "tests/libhello.so"])
        self._path_env.cmd([gcc, "tests/hello.c", "tests/libhello.so",
                            "-o", "tests/hello"])
        # Check that the executable runs natively as well as under sel_ldr.
        subprocess.check_call(["./tests/hello"])
        self._path_env.cmd(["ncrewrite", "tests/libhello.so", "tests/hello"])
        write_fh, read_fh = make_fh_pair()
        self._run_dynamic(["./tests/hello"], stdout=write_fh)
        assert_equals(read_fh.read(), "Hello world, in libhello\n")

    def test_dlopen(self, log):
        self._path_env.cmd(["nacl-glibc-gcc", "tests/dlopen.c", "-ldl",
                            "-o", "tests/dlopen"])
        self._path_env.cmd(["ncrewrite", "tests/dlopen"])
        write_fh, read_fh = make_fh_pair()
        self._run_dynamic(["./tests/dlopen"], stdout=write_fh)
        assert_equals(read_fh.read(),
                      "Trying dlopen\nHello world, in libhello\nDone\n")

    def test_static_dlopen(self, log):
        self._path_env.cmd([
                "nacl-glibc-gcc", "-static",
                "-Wl,-T,glibc/elf_i386.x",
                "tests/dlopen.c", "-ldl", "-o", "tests/dlopen-static"])
        self._path_env.cmd(["ncrewrite", "tests/dlopen-static"])
        self._path_env.cmd(["ncval_stubout", "tests/dlopen-static"])
        write_fh, read_fh = make_fh_pair()
        self._path_env.cmd(["sel_ldr", "-d",
                            "-E", "LD_LIBRARY_PATH=install-stubout/lib",
                            "./tests/dlopen-static"], stdout=write_fh)
        # The output comes out in a different order from before
        # because the executable and library use different copies of
        # stdio, and thus different buffers for stdout.
        assert_equals(read_fh.read(),
                      "Hello world, in libhello\nTrying dlopen\nDone\n")

    def test_toolchain(self, log):
        self._path_env.cmd(["python", "toolchain_test.py"])

    def test_gcc_wrapper(self, log):
        self._path_env.cmd(["python", "gccwrapper_test.py"])

    def test_python(self, log):
        self._path_env.cmd(["ncrewrite", "Python-2.6.1/python"])
        write_fh, read_fh = make_fh_pair()
        proc = self._run_dynamic(
            ["Python-2.6.1/python", "-c", 'print "Hello world!"'],
            stdout=write_fh)
        assert_equals(read_fh.read(), "Hello world!\n")

    def test_debugger(self, log):
        self._path_env.cmd(["nacl-glibc-gcc", "-g", "tests/faulty.c",
                            "-o", "tests/faulty"])
        self._path_env.cmd(["ncrewrite", "tests/faulty"])
        write_fh, read_fh = make_fh_pair()
        self._path_env.cmd(["python", "debugger.py", "tests/faulty"],
                           stdout=write_fh)
        stdout = read_fh.read()
        expected = ("Caught signal\n",
                    "tests/faulty.c:5\n",
                    "csu/libc-start.c",
                    "Signal handler done, exiting\n")
        print stdout,
        for line in expected:
            if line not in stdout:
                raise AssertionError("Text %r not found in output" % line)
        print "** Debug output OK"

    @action_tree.action_node
    def build(self):
        return [self.build_toolchain,
                self.build_nacl,
                self.configure_glibc,
                self.build_glibc,
                self.install_glibc,
                self.stub_out,
                self.build_sqlite,
                self.configure_python,
                self.build_python,
                self.install_python,
                self.install_sqlite,
                # self.build_python_extension, # TODO: re-enable
                ]

    @action_tree.action_node
    def test(self):
        return [self.test_gcc_wrapper,
                self.test_nacl,
                self.test_toolchain,
                self.test_glibc_static,
                self.test_running_ldso,
                self.test_glibc_dynamic,
                self.test_libraries,
                self.test_dlopen,
                self.test_static_dlopen,
                self.test_python,
                self.test_debugger]

    @action_tree.action_node
    def all(self):
        return [self.checkout, self.build, self.test]


class DryRunEnv(object):

    def cmd(self, args, **kwargs):
        print " ".join(args)


def main(args):
    parser = optparse.OptionParser()
    parser.add_option("--dry-run", action="store_true", dest="dry_run",
                      help="Print commands instead of running them")
    options, args = parser.parse_args(args)
    if options.dry_run:
        env = DryRunEnv()
    else:
        env = cmd_env.BasicEnv()
    tree = NaClBuild(env, os.getcwd())
    action_tree.action_main(tree.all, args)


if __name__ == "__main__":
    main(sys.argv[1:])
