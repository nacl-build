
# This is a *very* minimal version of gclient (which can be found at
# http://src.chromium.org/svn/trunk/tools/depot_tools/).

import subprocess


def read_file(filename):
    fh = open(filename, "r")
    try:
        return fh.read()
    finally:
        fh.close()


def read_deps_file(filename):
    scope = {}
    def Var(varname):
        return scope["vars"][varname]
    scope["Var"] = Var
    exec read_file(filename) in scope
    return scope


def main():
    scope = read_deps_file("native_client/DEPS")
    for path, svn_url in sorted(scope["deps"].iteritems()):
        if not svn_url.startswith("http://nativeclient.googlecode.com/svn/trunk"):
            args = ["svn", "checkout", svn_url, path]
            print args
            subprocess.check_call(args)


if __name__ == "__main__":
    main()
