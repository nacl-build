
#include <stdio.h>


void foo(void)
{
  printf("Hello world, in libhello\n");
  /* This is for the static-dlopen case. */
  fflush(stdout);
}

void hello(void)
{
  /* Calls via PLT */
  foo();
}
