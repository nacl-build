
        /* glibc outputs some functions into .text.compat.  It aims to
           keep little-used functions together. */
        .section .text.compat

        /* TODO: Alignment should be set automatically whenever
           outputting instructions. */
        .p2align 5

        /* Define a symbol so that we can find the code in the output
           when debugging this test. */
        .global foo
foo:

        /* All that matters here is that we output an odd number of
           bytes.  If this gets padded with zeroes, it will fail to
           validate.  That's because zeroes decode to two-byte
           instructions. */
        hlt
