
int main()
{
  /* Cause a segfault. */
  *(int *) 0 = 0;
  __asm__("hlt");
  return 0;
}
