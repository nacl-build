
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main()
{
  printf("Trying dlopen\n");
  void *handle = dlopen("tests/libhello.so", RTLD_LAZY);
  if(handle == NULL) {
    printf("%s", dlerror());
    return 1;
  }
  void (*func)(void) = dlsym(handle, "hello");
  func();
  dlclose(handle);
  printf("Done\n");
  return 0;
}
