
import os
import subprocess

import lxml.etree


deps_file = "deps.xml"


def get1(lst):
    assert len(lst) == 1, lst
    return lst[0]


def write_file_atomically(filename, data):
    tmp_filename = "%s.tmp" % filename
    fh = open(tmp_filename, "w")
    fh.write(data)
    fh.close()
    os.rename(tmp_filename, filename)


def get_modules():
    xml = lxml.etree.parse(open(deps_file))
    for mod in xml.xpath(".//module"):
        yield {"name": mod.attrib["name"],
               "path": get1(mod.xpath(".//path")).attrib["dir"],
               "commit": get1(mod.xpath(".//rev")).attrib["commit"],
               "url": get1(mod.xpath(".//repo")).attrib["url"],
               }


def main():
    xml = lxml.etree.parse(open(deps_file))
    for mod in xml.xpath(".//module"):
        path = get1(mod.xpath(".//path"))
        proc = subprocess.Popen(
            ["git", "rev-parse", "HEAD"], cwd=path.attrib["dir"],
            stdout=subprocess.PIPE)
        stdout = proc.communicate()[0]
        assert proc.wait() == 0
        commit = stdout.rstrip()
        rev = get1(mod.xpath(".//rev"))
        rev.attrib["commit"] = commit
        print commit, mod.attrib["name"]
    write_file_atomically(deps_file, lxml.etree.tostring(xml) + "\n")


if __name__ == "__main__":
    main()
