
import subprocess
import unittest


class ToolchainTests(unittest.TestCase):

    def test_padding_unusual_code_sections(self):
        # Test that ends of code sections are padded with NOPs not
        # zeroes when concatenated by the linker.
        subprocess.check_call(["nacl-gcc", "-c", "tests/section-padding.S",
                               "-o", "tests/section-padding-1.o"])
        subprocess.check_call(["nacl-gcc", "-c", "tests/section-padding.S",
                               "-Dfoo=foo2",
                               "-o", "tests/section-padding-2.o"])
        # Use of --relocatable mimics how glibc links libc.so in two
        # steps, producing libc_pic.os as an intermediate object.
        subprocess.check_call(["nacl-ld", "--relocatable",
                               "-Lglibc/ld",
                               "tests/section-padding-1.o",
                               "tests/section-padding-2.o",
                               "-o", "tests/section-padding-both.o"])
        # Link into an .so so that we can run ncval on it.
        # TODO: Make ncval work on .o files.
        subprocess.check_call(["nacl-gcc", "-shared",
                               "-Lglibc/ld",
                               "tests/section-padding-both.o",
                               "-o", "tests/section-padding-both.so"])
        subprocess.check_call(["ncval", "tests/section-padding-both.so"])


if __name__ == "__main__":
    unittest.main()
