
# This is a wrapper for sel_ldr that annotates the backtrace it
# outputs with source code info.

import re
import subprocess
import sys


def addr2line(obj_file, addr, indent=""):
    proc = subprocess.Popen(["addr2line", "-f", "-e", obj_file, "%x" % addr],
                            stdout=subprocess.PIPE)
    for line in proc.stdout:
        print indent + line,
    assert proc.wait() == 0


def main(argv):
    executable = argv[0]
    maps = [{"base": 0x20000, "offset": 0x20000, "size": 0x2d000,
             "path": "install-stubout/lib/ld-linux.so.2", "file": "ld.so"}]
    # TODO: don't assume executable's load address and size
    maps.append({"base": 0x1000000, "offset": 0, "size": 0x20000,
                 "path": executable, "file": executable})

    def match():
        last_path = "?"
        last_file = "?"
        while True:
            line = yield
            m = re.search("trying file=(.*)", line)
            if m is not None:
                last_path = m.group(1)

            m = re.search("file=(\S+).*generating link map", line)
            if m is not None:
                last_file = m.group(1)

            m = re.search("base: (\S+)\s+size: (\S+)", line)
            if m is not None:
                # base value assumes this is an ET_DYN object.
                info = {"base": int(m.group(1), 0),
                        "offset": int(m.group(1), 0),
                        "size": int(m.group(2), 0),
                        "path": last_path,
                        "file": last_file}
                maps.append(info)

    args = ["sel_ldr",
            "-E", "LD_DEBUG=all",
            "-d",
            "install-stubout/lib/ld-linux.so.2",
            "--", "--library-path", "install-stubout/lib",
            ] + argv
    proc = subprocess.Popen(
        args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    matcher = match()
    matcher.send(None)

    def decode_addr(addr):
        for obj in maps:
            if obj["base"] <= addr < obj["base"] + obj["size"]:
                offset = addr - obj["offset"]
                print "    in %s: offset=%x" % (obj["path"], offset)
                addr2line(obj["path"], offset, "    ")

    for line in proc.stdout:
        print line,
        matcher.send(line)
        m = re.search("code_addr=(\S+)", line)
        if m is not None:
            decode_addr(int(m.group(1), 16))

    print "exit status:", proc.wait()
    print "libraries loaded:"
    fmt = "  %(base)08x-%(end)08x size=%(size)08x file=%(file)s path=%(path)s"
    for obj in sorted(maps, key=lambda obj: obj["base"]):
        obj["end"] = obj["base"] + obj["size"]
        print fmt % obj


if __name__ == "__main__":
    main(sys.argv[1:])
